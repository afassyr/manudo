#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This program merges tiles of a digital version of the Aberdeen Bestiary
downloaded from https://www.abdn.ac.uk/bestiary/
"""
import click
import logging
import os
import re

from PIL import Image


logging.basicConfig(
    filename="manudo.log",
    level=logging.DEBUG,
    format='%(asctime)s (%(threadName)-2s) %(message)s',
)

IMAGE_DIR = "./aberdeen_bestiary/"
BASE_IMG_NAME = "base.jpg"
# Create a base image on which tiles will be pasted
# The values of x and y:
# x = (16 * 256) + 198
# y = (25 * 256) + 100
base_image = Image.new("RGB", (4294, 6500))
base_image.save(IMAGE_DIR + BASE_IMG_NAME)
# Copy the base image
base_width, base_height = base_image.size


def get_xy(tile):
    """
    Gets column and row (x and y) from tile names. E.g. "5-0-0" means
    a tile is meant to be positioned in the first column and the first row
    in a folio.
    """
    m = re.match(r"(\w+_\d+_\w+)_(tg\d+)_5-(\d+)-(\d+).jpg", tile)
    x, y = m.group(3), m.group(4)
    return x, y


def calculate_pos(tile_size, x, y):
    """
    Calculates position of a tile measured in pixels, derived from dimensions
    of the tile and the base image. Also returns width and height of a tile.
    """
    width, height = tile_size
    left = int(x) * width
    top = int(y) * height
    if width == 256 and height < 256:
        top = base_height - height
    elif width < 256 and height == 256:
        left = base_width - width
    elif (width < 256 and height < 256) and (int(x) == 16 and int(y) == 25):
        left = base_width - width
        top = base_height - height
    return width, height, left, top


def merge(folio):
    base_image = Image.open(IMAGE_DIR + BASE_IMG_NAME)
    tiles = os.listdir("{}{}/".format(IMAGE_DIR, folio))
    with click.progressbar(
        tiles,
        label="[*] Merging tiles for {}:".format(folio),
    ) as bar:
        for tile in bar:
            if tile.endswith('.jpg'):
                try:
                    x, y = get_xy(tile)
                except AttributeError:
                    break
                try:
                    tile_image = Image.open("{}{}/{}".format(
                        IMAGE_DIR, folio, tile)
                    )
                    width, height, left, top = calculate_pos(
                        tile_image.size, x, y
                    )
                    base_image.paste(tile_image, (left, top))
                except OSError:
                    logging.debug(
                        """
Could not identify image file {}{}/{}.
It might have been corrupted. Try to download it again.
                        """.format(IMAGE_DIR, folio, tile)
                    )
            else:
                continue
        # Save the base image as a new image containing all the tiles.
        base_image.save("{}{}.jpg".format(IMAGE_DIR, folio))
        bar.update(1)


def ask_to_overwrite(folio, all=False):
    user_answer = input("[*] Folio {}.jpg exists. Overwrite [y/n]: ".format(
        folio
    ))
    if user_answer in ("y", "Y", "yes", "Yes", "YES"):
        return True
    elif user_answer in ("n", "N", "no", "No", "NO"):
        if all:
            print("[*] Not overwriting {}. Continuing".format(folio))
        else:
            print("[*] Exiting program")
        return False
    else:
        print("[*] Sorry, wrong answer")
        print("[*] Please answer either 'y' or 'n'")
        if all:
            pass
        else:
            print("[*] Exiting program")


def foliofile_exists(folio):
    foliofile = folio + ".jpg"
    if foliofile in os.listdir(IMAGE_DIR):
        return True
    else:
        return False


# On to actual merging now...
# Open a clean base image...
@click.command()
@click.argument("folio")
def main(folio):
    """
    This program merges tiles of a digital version of the Aberdeen Bestiary,
    downloaded from https://www.abdn.ac.uk/bestiary/, into high resolution
    images.

    NAME should be name of a folio or 'all'.

    The former should have a following structure: 'folio-N-r|v' (N is a number
    in range 1-103 and 'r|v' is either 'r' for 'recto' or 'v' for 'verso'.
    """
    if folio == "all":
        for dir in os.scandir(IMAGE_DIR):
            if dir.is_dir():
                folio = dir.name
                if foliofile_exists(folio):
                    if ask_to_overwrite(folio):
                        merge(folio)
                else:
                    merge(folio)
    else:
        if foliofile_exists(folio):
            if ask_to_overwrite(folio):
                merge(folio)
        else:
            merge(folio)


if __name__ == "__main__":
    main()
