#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
This program downloads tiles of a digital version of the Aberdeen Bestiary:
https://www.abdn.ac.uk/bestiary/
"""
from __future__ import print_function
from itertools import product

import click
import concurrent.futures
import os
import logging
import requests
import sys


logging.basicConfig(
    filename="manudo.log",
    level=logging.DEBUG,
    format='%(asctime)s (%(threadName)-2s) %(message)s',
)

BASE_URL = "https://www.abdn.ac.uk/bestiary/zoomify/"
FOLIO_PARAMS = [
    range(1, 104),
    ["r", "v"],
]
COORDINATES = [(col, row) for col in range(0, 17) for row in range(0, 26)]

# TODO: add some condition and/or check to the following line of code:
os.makedirs("aberdeen_bestiary", exist_ok=True)

def generate_tile_group(x, y):
    """
    Generates number of a tile group which a tile belongs to.
    """
    if y < 5 or (x in range(0, 2) and y == 5):
        tile_group = 0
    elif (y >= 5 and y < 20) \
        and not (x in range(0, 2) and y == 5) \
        or (x in range(0, 3) and y == 20):
        tile_group = 1
    else:
        tile_group = 2
    return tile_group

def generate_tile_url(folio_num, side, coordinates):
    """
    Generates a URL for a tile
    """
    x, y = coordinates[0], coordinates[1]
    tile_group = generate_tile_group(x, y)
    tile_url = "f{}{}/TileGroup{}/5-{}-{}.jpg".format(folio_num,
                                                    side,
                                                    tile_group,
                                                    x, y)
    tilename = "folio_{}_{}_tg{}_5-{}-{}.jpg".format(folio_num,
                                                    side,
                                                    tile_group,
                                                    x, y)
    tile_url = BASE_URL + tile_url
    return tile_url, tilename

def get_tile(url, session):
    try:
        response = session.get(url)
    except Exception as e:
        logging.debug("Request for %r generated exception %r" % (url, e))
    else:
        return response

def save_tile(response, tile_dir, tilename):
    if response.status_code == requests.codes.ok:
        # Prepare an image file
        image_file = open(os.path.join(tile_dir, tilename), "wb")
        try:
            response.raise_for_status()
        except Exception as exc:
            print("[*] There was a problem: {}".format(exc))
        # Save an image
        for chunk in response.iter_content(100000):
            image_file.write(chunk)
        image_file.close()
    else:
        pass

executor = concurrent.futures.ThreadPoolExecutor(max_workers=20)
session = requests.Session()

def main():
    with click.progressbar(COORDINATES, label="Downloading the tiles:",
                           item_show_func=repr) as bar:
        try:
            for folio, side in product(*FOLIO_PARAMS):
                tile_dir = "aberdeen_bestiary/folio-{}-{}".format(folio, side)
                if not os.path.isdir(tile_dir):
                    os.makedirs(tile_dir, exist_ok=True)

                future_to_coord = {executor.submit(
                    generate_tile_url, folio, side, coord
                ): coord for coord in COORDINATES}
                for future in concurrent.futures.as_completed(future_to_coord):
                    coord = future_to_coord[future]
                    url, tilename = future.result()

                    image_file_exists = os.path.isfile(
                        os.path.join(tile_dir, tilename)
                    )
                    if image_file_exists:
                        continue
                    try:
                        response = get_tile(url, session)
                        save_tile(response, tile_dir, tilename)
                    except Exception as e:
                        logging.debug(
                            "%r generated an exception: %s" % (coord, e)
                        )
                    else:
                        logging.debug(
                            "Folio %r | %r | %r coordinates processed" %
                                (folio, side, str(coord))
                        )
                bar.update(1)
        except KeyboardInterrupt:
            print("\n\n[*] User requested an interrupt")
            print("[*] Aborting")
            sys.exit()

if __name__ == "__main__":
    main()
