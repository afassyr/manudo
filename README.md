# README #

Manudo is a very specialised web scraper. It downloads thousands of high resolution tiles
which, when put together, form particular folios of the Aberdeen Bestiary.

After getting all the images, another small program merges them into high resolution
folios.

## How to run the programs

### On Linux systems:

```
$ python3 downloader.py
Downloading the tiles:  [##----------------------------------]    5%  0d 00:01:56

$ python3 merger.py
Merging tiles:  [####################################]  100%
```

### On Windows

TODO.
